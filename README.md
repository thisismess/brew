# Brew
This repo contains some Homebrew formulae that aren't available in the official distribution. You can use `brew` to install them like so:

	brew install https://bitbucket.org/thisismess/brew/raw/master/goreman.rb
