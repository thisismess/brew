require 'formula'

class Goreman < Formula
  
  url 'https://github.com/mattn/goreman/releases/download/v0.0.6/goreman_darwin_amd64.zip'
  homepage 'https://github.com/mattn/goreman'
  version '1'
  
  def install
    bin.install("eg")
    bin.install("goreman")
  end
  
end
